# frozen_string_literal: true

module Dialog
  class Controller
    attr_accessor :game_controller,
                  :entries,
                  :and_then,
                  :shown,
                  :infrequent_showers

    LINE_LEFT_MARGIN = 2
    DIALOG_TOP_MARGIN = 16
    HINT_TOP_MARGIN = 44

    def initialize(opts = {})
      self.game_controller = opts[:game_controller]
      self.entries = []
      self.shown = []
      self.infrequent_showers = %w[LeaveSectorDay]
      load_dialog_tree('Intro')
    end

    def process(inputs)
      if InputController.next_dialog_pressed?(inputs)
        if entries[0][:type] == 'dialog'
          game_controller.play_sound('sounds/dialog_next.wav')
        elsif entries[0][:type] == 'hint'
          game_controller.play_sound('sounds/hint_hide.wav')
        end
        self.entries = if entries.length > 1
                         entries[1..entries.length - 1]
                       else
                         reset_or_next
                       end
      end
    end

    def reset_or_next
      if and_then.nil?
        self.entries = []
      else
        load_dialog_tree(and_then)
      end
      entries
    end

    def load_dialog_tree(tree, opts = {})
      return if shown?(tree)

      dialog_event = Object.const_get("Dialog::#{tree}")
      self.entries = dialog_event.entries(opts)
      self.and_then = dialog_event.and_then
      shown << tree if one_timers.include?(tree) || infrequent_showers.include?(tree)
      game_controller.play_sound('sounds/hint_show.wav') if entries[0][:type] == 'hint'
    end

    def clear_infrequent
      infrequent_showers.each do |tree|
        self.shown = shown.reject { |t| t == tree }
      end
    end

    def sprites
      return unless entries.count.positive?

      [
        box_sprite,
        hint_sprite
      ]
    end

    def labels
      current_entry_text
    end

    def box_sprite
      if entries[0][:type] == 'dialog'
        [0, 0, 64, 26, '/sprites/dialog/dialog_box.png']
      elsif entries[0][:type] == 'hint'
        [0, 0, 64, 64, '/sprites/dialog/hint_box.png']
      end
    end

    def hint_sprite
      if entries[0][:type] == 'dialog'
        if entries[0][:icon_pos] == 'left'
          [4, 23, 6, 6, entries[0][:icon]]
        else
          [54, 23, 6, 6, entries[0][:icon]]
        end
      elsif entries[0][:type] == 'hint'
        [28, 52, 8, 8, entries[0][:icon]]
      end
    end

    def current_entry_text
      return unless entries[0]

      words = entries[0][:text].upcase.split(' ')
      lines = []
      line = ''
      words.each do |word|
        if (line.length + word.length + 1) < max_line_length
          line = line.length.zero? ? word : "#{line} #{word}"
        else
          top_margin_for_entry
          line_left = LINE_LEFT_MARGIN + (((max_line_length - line.length) / 2).floor * 5)
          lines << [line_left, top_margin_for_entry - (lines.count * line_height), line, 44, 44, 44]
          line = word
        end
      end
      line_left = LINE_LEFT_MARGIN + (((max_line_length - line.length) / 2).floor * 5)
      lines << [line_left, top_margin_for_entry - (lines.count * line_height), line, 44, 44, 44] if line != ''
      lines
    end

    def top_margin_for_entry
      return HINT_TOP_MARGIN if entries[0][:type] == 'hint'

      DIALOG_TOP_MARGIN
    end

    def max_line_length
      @max_line_length ||= (66 / game_controller.game_manager.letter_spacing).floor
    end

    def line_height
      @line_height = game_controller.game_manager.line_height
    end

    def shown?(tree)
      shown.include?(tree)
    end

    def one_timers
      @one_timers ||= [
        'Intro',
        'Hints::ViewInventory',
        'Hints::Slime',
        'Hints::Skeleton',
        'Hints::RedSkeleton'
      ]
    end
  end
end
