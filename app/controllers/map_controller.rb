# frozen_string_literal: true

class MapController
  attr_accessor :game_controller, :map

  WIDTH_IN_SECTORS = 4
  HEIGHT_IN_SECTORS = 6
  MAP_WIDTH = WIDTH_IN_SECTORS * ::MapSector::WIDTH_IN_TILES
  MAP_HEIGHT = HEIGHT_IN_SECTORS * ::MapSector::HEIGHT_IN_TILES
  TILE_WIDTH = MapTile::WIDTH_IN_PIXELS
  TILE_HEIGHT = MapTile::HEIGHT_IN_PIXELS

  def initialize(opts = {})
    self.game_controller = opts[:game_controller]
  end

  def sprites
    map.sprites
  end

  def foreground_sprites
    map.foreground_sprites
  end

  def labels
    []
  end

  def load_map
    self.map = Map.new(
      game_controller: game_controller,
      map_controller: self,
      width_in_sectors: WIDTH_IN_SECTORS,
      height_in_sectors: HEIGHT_IN_SECTORS
    )
  end

  def get_tile(position)
    map.get_tile(position)
  end

  def get_sector(position)
    map.get_sector(position)
  end

  def screen_position
    game_controller.screen_position
  end

  def screen_width
    game_controller.screen_width
  end

  def screen_height
    game_controller.screen_height
  end

  def cycle
    game_controller.cycle
  end

  def screen_right
    @screen_right ||= (MAP_WIDTH * TILE_WIDTH) - game_controller.screen_width
  end

  def screen_bottom
    @screen_bottom ||= (MAP_HEIGHT * TILE_HEIGHT) - game_controller.screen_height
  end

  def left
    -4
  end

  def right
    @right ||= (MAP_WIDTH * TILE_WIDTH - 4)
  end

  def top
    0
  end

  def bottom
    @bottom ||= (MAP_HEIGHT * TILE_HEIGHT)
  end

  def tile_width
    TILE_WIDTH
  end

  def tile_height
    TILE_HEIGHT
  end

  def point_to_tile(point = {})
    {
      x: (point[:x] / TILE_WIDTH).floor,
      y: (point[:y] / TILE_HEIGHT).floor
    }
  end

  def point_to_sector(point = {})
    {
      x: (point[:x] / (TILE_WIDTH * ::MapSector::WIDTH_IN_TILES)).floor,
      y: (point[:y] / (TILE_HEIGHT * ::MapSector::HEIGHT_IN_TILES)).floor
    }
  end

  def clear_cache
    map.clear_cache
  end
end
