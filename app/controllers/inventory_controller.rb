# frozen_string_literal: true

class InventoryController
  attr_accessor :game_manager,
                :selected

  TOP_ROW = 36
  LEFT_EDGE = 0
  MAX_COLUMNS = 4

  def initialize(game_manager)
    self.game_manager = game_manager
    self.selected = 0
  end

  def process(labels, sprites, inputs)
    game_manager.show_inventory(false) if InputController.toggle_inventory_pressed?(inputs) ||
                                          InputController.secondary_pressed?(inputs) ||
                                          InputController.tertiary_pressed?(inputs)

    move_selector(selected + 1) if InputController.right_pressed?(inputs)
    move_selector(selected - 1) if InputController.left_pressed?(inputs)
    use_item if InputController.primary_pressed?(inputs)

    labels << [8, 56, 'INVENTORY']
    labels << [30, 2, "HP:#{game_manager.game_controller.player.hp}"]
    sprites << [0, 0, 64, 64, '/sprites/dialog/hint_box.png']
    column = 0
    row = 0
    inventory.items.values.each do |item|
      sprite_x = LEFT_EDGE + (((column * 2) + 1) * 8)
      sprite_y = TOP_ROW - (((row * 2) - 1) * 8)
      sprites << item.sprite_at(
        x: sprite_x,
        y: sprite_y
      )
      labels << [sprite_x - 1, sprite_y - 4, item.formatted_quantity.to_s]

      if column + 1 > MAX_COLUMNS
        column = 0
        row += 1
      else
        column += 1
      end
    end
    sprites << selector unless inventory.items.count.zero?
  end

  def inventory
    @inventory ||= game_manager.game_controller.player.inventory
  end

  def background
    [0, 0, 1280, 720, 16, 16, 16]
  end

  def move_selector(new_val)
    self.selected = [[0, new_val].max, inventory.items.count - 1].min
  end

  def use_item
    item = inventory.items.values[selected]
    return if game_manager.game_controller.player.full_hp?
    game_manager.game_controller.player.add_hp(item.effect)
    item.remove_quantity(1)
    inventory.items = inventory.items.reject { |_key, item| item.quantity.zero? }
    self.selected = 0
  end

  def selector
    selected_x = LEFT_EDGE + (((selected % MAX_COLUMNS) * 2) + 1) * 8
    selected_y = TOP_ROW - ((((selected / MAX_COLUMNS).floor * 2) - 1) * 8)
    [selected_x, selected_y, 8, 8, '/sprites/inventory_select.png']
  end
end
