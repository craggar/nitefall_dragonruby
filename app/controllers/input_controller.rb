# frozen_string_literal: true

class InputController
  def self.primary_held?(inputs)
    inputs.keyboard.key_held.z ||
      inputs.keyboard.key_held.j ||
      inputs.controller_one.key_held.b
  end

  def self.secondary_held?(inputs)
    inputs.keyboard.key_held.x ||
      inputs.keyboard.key_held.k ||
      inputs.controller_one.key_held.a
  end

  def self.tertiary_held?(inputs)
    inputs.keyboard.key_held.c ||
      inputs.keyboard.key_held.l ||
      inputs.controller_one.key_held.x
  end

  def self.quandary_held?(inputs)
    inputs.controller_one.key_held.y
  end

  def self.up_held?(inputs)
    inputs.keyboard.key_held.up ||
      inputs.keyboard.key_held.w ||
      inputs.controller_one.left_analog_y_perc.positive? ||
      inputs.controller_one.key_held.up
  end

  def self.down_held?(inputs)
    inputs.keyboard.key_held.down ||
      inputs.keyboard.key_held.s ||
      inputs.controller_one.left_analog_y_perc.negative? ||
      inputs.controller_one.key_held.down
  end

  def self.left_held?(inputs)
    inputs.keyboard.key_held.left ||
      inputs.keyboard.key_held.a ||
      inputs.controller_one.left_analog_x_perc.negative? ||
      inputs.controller_one.key_held.left
  end

  def self.right_held?(inputs)
    inputs.keyboard.key_held.right ||
      inputs.keyboard.key_held.d ||
      inputs.controller_one.left_analog_x_perc.positive? ||
      inputs.controller_one.key_held.right
  end

  def self.primary_pressed?(inputs)
    inputs.keyboard.key_down.z ||
      inputs.keyboard.key_down.j ||
      inputs.controller_one.key_down.b
  end

  def self.secondary_pressed?(inputs)
    inputs.keyboard.key_down.x ||
      inputs.keyboard.key_down.k ||
      inputs.controller_one.key_down.a
  end

  def self.tertiary_pressed?(inputs)
    inputs.keyboard.key_down.c ||
      inputs.keyboard.key_down.l ||
      inputs.controller_one.key_down.x
  end

  def self.quandary_pressed?(inputs)
    inputs.controller_one.key_down.y
  end

  def self.up_pressed?(inputs)
    inputs.keyboard.key_down.up ||
      inputs.keyboard.key_down.w ||
      inputs.controller_one.key_down.up
  end

  def self.down_pressed?(inputs)
    inputs.keyboard.key_down.down ||
      inputs.keyboard.key_down.s ||
      inputs.controller_one.key_down.down
  end

  def self.left_pressed?(inputs)
    inputs.keyboard.key_down.left ||
      inputs.keyboard.key_down.a ||
      inputs.controller_one.key_down.left
  end

  def self.right_pressed?(inputs)
    inputs.keyboard.right ||
      inputs.keyboard.key_down.d ||
      inputs.controller_one.key_down.right
  end

  def self.next_dialog_pressed?(inputs)
    secondary_pressed?(inputs) ||
      tertiary_pressed?(inputs) ||
      inputs.keyboard.key_down.space ||
      inputs.keyboard.key_down.enter
  end

  def self.next_intro_pressed?(inputs)
    next_dialog_pressed?(inputs) ||
      primary_pressed?(inputs)
  end

  def self.menu_select_pressed?(inputs)
    primary_pressed?(inputs) ||
      inputs.keyboard.key_down.space ||
      inputs.keyboard.key_down.enter
  end

  def self.toggle_menu_pressed?(inputs)
    inputs.keyboard.key_down.escape ||
      inputs.controller_one.key_down.start
  end

  def self.toggle_inventory_pressed?(inputs)
    inputs.keyboard.key_down.close_square_brace ||
      quandary_pressed?(inputs)
  end

  def self.show_mini_map_pressed?(inputs)
    inputs.keyboard.key_down.m ||
      tertiary_pressed?(inputs)
  end

  def self.close_mini_map_pressed?(inputs)
    inputs.keyboard.key_down.escape ||
      inputs.keyboard.key_down.m ||
      inputs.keyboard.key_down.space ||
      primary_pressed?(inputs) ||
      secondary_pressed?(inputs) ||
      tertiary_pressed?(inputs)
  end
end
