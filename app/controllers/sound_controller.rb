# frozen_string_literal: true

class SoundController
  attr_accessor :music_queued, :music, :sounds, :sounds_enabled, :music_enabled

  def initialize
    self.sounds_enabled = true
    self.music_enabled = true
    self.sounds = []
  end

  def process(outputs)
    sounds.each { |s| outputs.sounds << s } if sounds_enabled
    clear_sounds
    outputs.sounds << 'silent.ogg' unless music_enabled
    return unless music_queued

    outputs.sounds << music if music_enabled && music
    self.music_queued = false
  end

  def play_sound(sound)
    return if sound.split('.').last == 'ogg'

    sounds << sound unless sounds.include?(sound)
  end

  def set_music(music)
    return unless music.split('.').last == 'ogg'

    self.music = music
    self.music_queued = true
  end

  def clear_sounds
    self.sounds = []
  end

  def toggle_music
    self.music_enabled = !music_enabled
    self.music_queued = music_enabled
  end

  def toggle_sound
    self.sounds_enabled = !sounds_enabled
  end
end
