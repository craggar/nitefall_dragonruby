# frozen_string_literal: true

class MiniMapController
  attr_accessor :game_controller

  TILE_WIDTH = 3
  TILE_HEIGHT = 3

  def initialize(game_controller)
    self.game_controller = game_controller
  end

  def process(_labels, _sprites, inputs)
    if close_mini_map?(inputs)
      game_controller.hide_mini_map
      return
    end
    sprites.each { |sprite| _sprites << sprite }
    _sprites << goal_sprite
    _sprites << player_sprite
    _labels << goal_label
    _labels << player_label
  end

  def close_mini_map?(inputs)
    InputController.close_mini_map_pressed?(inputs)
  end

  def sprites
    map.flattened_sectors.select(&:visited).each.map do |sector|
      next unless sector.visited

      tile_id = sector.type < 10 ? "0#{sector.type}" : sector.type
      [left_edge + (sector.sector_x * TILE_WIDTH), bottom_edge + (sector.sector_y * TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT, "/sprites/tiles/mini_tile_#{tile_id}.png"]
    end
  end

  def player_sprite
    sector_x = game_controller.player.sector[:x]
    sector_y = game_controller.player.sector[:y]
    [left_edge + (sector_x * TILE_WIDTH), bottom_edge + (sector_y * TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT, '/sprites/tiles/mini_tile_player.png']
  end

  def player_label
    [40, 2, 'you', 255, 0, 255]
  end

  def goal_sprite
    sector_x = game_controller.map_controller.map.goal_sector&.sector_x
    sector_y = game_controller.map_controller.map.goal_sector&.sector_y
    [left_edge + (sector_x * TILE_WIDTH), bottom_edge + (sector_y * TILE_HEIGHT), TILE_WIDTH, TILE_HEIGHT, '/sprites/tiles/mini_tile_goal.png']
  end

  def goal_label
    [2, 2, 'goal', 255, 0, 0]
  end

  def left_edge
    @left_edge ||= 32 - ((map.width_in_sectors / 2) * TILE_WIDTH)
  end

  def bottom_edge
    @bottom_edge ||= 32 - ((map.height_in_sectors / 2) * TILE_HEIGHT)
  end

  def map
    game_controller.map_controller.map
  end

  def background
    [0, 0, 1280, 720, 16, 16, 16]
  end
end
