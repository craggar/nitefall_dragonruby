# frozen_string_literal: true

class IntroController
  attr_accessor :game_manager,
                :timer,
                :local_tick,
                :sprite_index,
                :cycle_counter

  def initialize(game_manager)
    self.game_manager = game_manager
    self.sprite_index = 0
    self.local_tick = 0
    self.cycle_counter = 0
  end

  def process(_labels, _sprites, inputs)
    game_manager.show_menu unless sprite_index < images.count
    increment_local_tick
    _sprites << sprite
    next_sprite if skip_pressed?(inputs)
  end

  def skip_pressed?(inputs)
    InputController.next_intro_pressed?(inputs)
  end

  def next_sprite
    self.sprite_index += 1
    self.local_tick = 0
    self.cycle_counter = 0
  end

  def increment_local_tick
    if local_tick + 1 == 60
      self.local_tick = 0
      if cycle_counter + 1 == 3
        self.cycle_counter = 0
        self.sprite_index += 1
      else
        self.cycle_counter += 1
      end
    else
      self.local_tick += 1
    end
  end

  def sprite
    return [] unless sprite_index < images.count

    [0, 0, 64, 64, images[sprite_index]]
  end

  def images
    @images ||= [
      '/sprites/intro/console-logo.png'
    ]
  end

  def background
    [0, 0, 1280, 720, 16, 16, 16]
  end
end
