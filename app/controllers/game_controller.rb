# frozen_string_literal: true

class GameController
  attr_accessor :game_manager,
                :map_controller,
                :dialog_controller,
                :draw_outlines,
                :player,
                :enemies,
                :screen_position,
                :screen_sector,
                :cycle,
                :cycle_counter,
                :local_tick,
                :in_progress,
                :day_begin_sector,
                :pickups,
                :day_count,
                :cycle_length

  SCREEN_WIDTH = 64
  SCREEN_HEIGHT = 64
  HALF_SCREEN_WIDTH = 32
  HALF_SCREEN_HEIGHT = 32

  def initialize(game_manager)
    self.game_manager = game_manager
    self.map_controller = ::MapController.new(game_controller: self)
    self.dialog_controller = ::Dialog::Controller.new(game_controller: self)
    self.draw_outlines = false
    self.cycle_counter = 0
    self.cycle_length = 10
    self.local_tick = 0
    self.in_progress = false
    self.day_count = 0
    self.pickups = []
  end

  def start
    self.in_progress = true
    self.screen_position = { x: 0, y: 0 }
    generate_map
    place_player
    scatter_pickups
    place_npcs

    set_cycle(:day, skip_dialog: true)
  end

  def generate_map
    map_controller.load_map
  end

  def place_player
    self.player = ::Player.new(
      game_controller: self,
      map_controller: map_controller,
      x: map_controller.map.player_spawn_sector.world_position[:x] + 32,
      y: map_controller.map.player_spawn_sector.world_position[:y] + 32
    )
  end

  def scatter_pickups
    self.pickups = []

    pickups << ::Pickups::Apple.new(
      game_controller: self,
      map_controller: map_controller,
      x: map_controller.map.player_spawn_sector.world_position[:x] + 40,
      y: map_controller.map.player_spawn_sector.world_position[:x] + 40,
    )
  end

  def place_npcs
    self.enemies = []
    map_controller.map.flattened_sectors.each do |sector|
      next if sector.difficulty.zero?

      valid_tiles = sector.flattened_tiles.reject { |t| t.type == ::MapTile::TILE_TYPES[:BLOCKING] }
      sector.enemy_spawn_count.times do
        place = valid_tiles.sample
        enemy_type = [::Slime, ::Skeleton, ::RedSkeleton][0..sector.difficulty - 1].sample
        spawn_enemy(
          enemy_type,
          x: place.world_position[:x],
          y: place.world_position[:y]
        )
      end
    end
  end

  def spawn_random_pickup(position)
    pickup_type = [::Pickups::Apple, ::Pickups::Water].sample
    pickups << pickup_type.new(
      game_controller: self,
      map_controller: map_controller,
      x: position[:x],
      y: position[:y],
    )
  end

  def spawn_enemy(enemy_class, position)
    enemies << enemy_class.new(
      game_controller: self,
      map_controller: map_controller,
      x: position[:x],
      y: position[:y]
    )
  end

  def process(labels, sprites, inputs)
    return if show_mini_map?(inputs)

    if player.hp.zero?
      game_manager.game_over
      return
    end

    if dialog_controller.entries&.count&.zero?
      increment_local_tick
      entities_process(inputs)
    else
      dialog_controller.process(inputs)
    end

    process_inputs(inputs)

    scroll_screen

    load_sprites(sprites)
    load_labels(labels)
  end

  def entities_process(inputs)
    player.process(inputs)
    if nite?
      enemies.each do |enemy|
        enemy.process(inputs)
      end
    end
    self.enemies = enemies.reject do |e|
      spawn_random_pickup(x: e.x, y: e.y) if e.dead?
      e.dead?
    end
    pickups.select(&:visible).each(&:process)
    self.pickups = pickups.reject do |pickup|
      if pickup.picked_up
        player.pick_up(pickup, 1)
        load_dialog_tree('Hints::ViewInventory')
      end
      pickup.picked_up
    end
  end

  def scroll_screen
    self.screen_position = {
      x: [[0, player.x - HALF_SCREEN_WIDTH].max, map_controller.screen_right].min,
      y: [[0, player.y - HALF_SCREEN_HEIGHT].max, map_controller.screen_bottom].min
    }

    return unless local_tick.zero?

    self.screen_sector = {
      x: ((screen_position[:x] / MapTile::WIDTH_IN_PIXELS) / ::MapSector::WIDTH_IN_TILES).floor,
      y: ((screen_position[:y] / MapTile::HEIGHT_IN_PIXELS) / ::MapSector::HEIGHT_IN_TILES).floor
    }
  end

  def load_sprites(sprites)
    map_controller.sprites.each { |s| sprites << s }
    pickups.select(&:visible).each { |p| sprites << p.sprite }
    if nite?
      enemies.select(&:visible).each do |e|
        sprites << e.sprite
        # sprites << e.damager.sprite if e.damager
      end
    end

    sprites << player.sprite
    # sprites << player&.damager&.sprite

    # foreground sprites
    map_controller.foreground_sprites.each { |s| sprites << s }

    sprites << fog_sprite if day?

    return unless dialog_controller.entries&.count&.positive?

    dialog_controller.sprites.each { |s| sprites << s }
  end

  def load_labels(labels)
    if dialog_controller.entries&.count&.positive?
      dialog_controller.labels.each { |l| labels << l }
    else
      player.labels.each { |l| labels << l }
      enemies.select(&:visible).each do |enemy|
        enemy.labels.each { |l| labels << l }
      end
    end
  end

  def show_mini_map?(inputs)
    return false unless InputController.show_mini_map_pressed?(inputs)

    game_manager.show_mini_map(true)
    true
  end

  def hide_mini_map
    game_manager.show_mini_map(false)
  end

  def process_inputs(inputs)
    game_manager.show_menu if InputController.toggle_menu_pressed?(inputs)
    game_manager.show_inventory(true) if InputController.toggle_inventory_pressed?(inputs)
  end

  def play_sound(sound)
    sound_controller.play_sound(sound)
  end

  def screen_width
    SCREEN_WIDTH
  end

  def screen_height
    SCREEN_HEIGHT
  end

  def increment_local_tick
    if local_tick + 1 == 60
      self.local_tick = 0
      if cycle_counter + 1 == cycle_length
        self.cycle_counter = 0
        set_cycle cycle == :day ? :nite : :day
      else
        self.cycle_counter += 1
      end
    else
      self.local_tick += 1
    end
  end

  def fog_sprite
    image_i = [4, (local_tick / 15).floor % 4].min
    [0, 0, 64, 64, "/sprites/foreground/fog_0#{image_i}.png"]
  end

  def sound_controller
    game_manager.sound_controller
  end

  def set_cycle(new_cycle, opts = {})
    self.cycle = new_cycle
    self.day_count += 1 if new_cycle == :day
    sound_controller.set_music("music/#{cycle}_cycle.ogg")
    if opts[:skip_dialog] != true
      cycle == :day ? load_dialog_tree('Daybreak', day: day_count) : load_dialog_tree('Nitefall', day: day_count)
    end
    self.cycle_length = new_cycle == :day ? 10 : 20
    return unless cycle == :day

    self.day_begin_sector = map_controller.map.sectors[player.sector[:x]][player.sector[:y]]
    day_begin_sector.visited = true
    dialog_controller.clear_infrequent
    enemies.each { |e| e.cached_visible = nil }
    pickups.each { |p| p.cached_visible = nil }
    map_controller.clear_cache
  end

  def update_visited_sectors
    if map_controller.map.sectors[player.sector[:x]][player.sector[:y]] == map_controller.map.goal_sector
      game_manager.victory
    end
    return unless nite?

    map_controller.map.sectors[player.sector[:x]][player.sector[:y]].visited = true
  end

  def day?
    cycle == :day
  end

  def nite?
    cycle == :nite
  end

  def still_in_sector(new_x, new_y)
    new_sector = map_controller.point_to_sector(x: new_x, y: new_y)
    new_sector[:x] == day_begin_sector.sector_x &&
      new_sector[:y] == day_begin_sector.sector_y
  end

  def load_dialog_tree(tree, opts = {})
    dialog_controller.load_dialog_tree(tree, opts)
  end

  def background
    [0, 0, 1280, 720, 255, 255, 255]
  end
end
