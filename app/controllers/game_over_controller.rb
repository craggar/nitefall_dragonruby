# frozen_string_literal: true

class GameOverController
  attr_accessor :game_manager,
                :timer,
                :local_tick,
                :sprite_index,
                :cycle_counter,
                :pause_count

  def initialize(game_manager)
    self.game_manager = game_manager
    self.sprite_index = 0
    self.local_tick = 0
    self.cycle_counter = 0
    self.pause_count = 0
  end

  def process(labels, sprites, inputs)
    sprites << background_sprite
    sprites << player_sprite
    increment_local_tick

    return unless game_manager.game_controller.dialog_controller.entries.count.positive?

    game_manager.game_controller.dialog_controller.process(inputs)
    if game_manager.game_controller.dialog_controller.entries.count.zero?
      game_manager.show_menu
    else
      game_manager.game_controller.dialog_controller.sprites.each { |s| sprites << s }
      game_manager.game_controller.dialog_controller.labels.each { |l| labels << l }
    end
  end

  def background_sprite
    [0, 0, 64, 64, images[sprite_index]]
  end

  def player_sprite
    if local_tick < 30
      [30, 16, 8, 10, 'sprites/player/idle/player_idle_up_00.png']
    else
      [30, 16, 8, 10, 'sprites/player/idle/player_idle_up_01.png']
    end
  end

  def images
    @images ||= [
      '/sprites/game_over.png'
    ]
  end

  def background
    [0, 0, 1280, 720, 16, 16, 16]
  end

  def increment_local_tick
    if local_tick + 1 == 60
      self.local_tick = 0

      game_manager.game_controller.load_dialog_tree('GameOver') if pause_count + 1 == 4
      self.pause_count += 1
    else
      self.local_tick += 1
    end
  end
end
