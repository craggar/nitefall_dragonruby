# frozen_string_literal: true

class MenuController
  attr_accessor :game_manager, :selected_index, :pointer, :state, :ctrl_state

  OPTION_SPACING = 8
  TEXT_LEFT_EDGE = 8
  POINTER_LEFT_EDGE = TEXT_LEFT_EDGE - 6
  CENTRE_Y = 34
  CONTROLLER_STATES = [:show_keys_a, :show_keys_b, :show_xbox, :show_ps4].freeze

  def initialize(game_manager)
    self.game_manager = game_manager
    self.selected_index = 0
    self.state = :main_menu
  end

  def process(_labels, _sprites, inputs)
    if [:show_keys_a, :show_keys_b, :show_xbox, :show_ps4].include?(ctrl_state)
      self.ctrl_state = nil if InputController.menu_select_pressed?(inputs)
    else
      if InputController.up_pressed?(inputs)
        move_up
      elsif InputController.down_pressed?(inputs)
        move_down
      elsif InputController.menu_select_pressed?(inputs)
        select_option
      elsif InputController.toggle_menu_pressed?(inputs) && game_manager&.game_controller&.in_progress
        game_manager.resume_game
        self.state = :game_menu
        self.selected_index = 0
      end
    end
    labels.each { |l| _labels << l }
    sprites.each { |s| _sprites << s }
  end

  def select_option
    play_sound('sounds/menu_select.wav')
    if options[selected_index] == 'Play'
      self.state = :game_menu
      game_manager.start_game
    elsif options[selected_index] == 'Resume'
      game_manager.resume_game
    elsif options[selected_index] == 'Sound'
      self.state = :sound_menu
      self.selected_index = 0
    elsif options[selected_index] == 'Back'
      self.state = game_manager&.game_controller&.in_progress ? :game_menu : :main_menu
      self.selected_index = 0
    elsif options[selected_index].include? 'Keys'
      game_manager.cycle_input
      self.ctrl_state = CONTROLLER_STATES[game_manager.input_choice]
    elsif options[selected_index].include? 'Music:'
      sound_controller.toggle_music
    elsif options[selected_index].include? 'SFX:'
      sound_controller.toggle_sound
    elsif options[selected_index] == 'Quit'
      game_manager.quit
      self.selected_index = 0
    end
  end

  def move_up
    self.selected_index = if (selected_index - 1).negative?
                            self.selected_index = options.count - 1
                          else
                            self.selected_index -= 1
                          end
    play_sound('sounds/menu_scroll.wav')
  end

  def move_down
    self.selected_index = if selected_index + 1 == options.count
                            self.selected_index = 0
                          else
                            self.selected_index += 1
                          end
    play_sound('sounds/menu_scroll.wav')
  end

  def labels
    return [] if [:show_keys_a, :show_keys_b, :show_xbox, :show_ps4].include?(ctrl_state)
    opt_idx = 0
    options.map do |option|
      label = [TEXT_LEFT_EDGE, top - (opt_idx * OPTION_SPACING), option, 226, 226, 255]
      opt_idx += 1
      label
    end
  end

  def top
    CENTRE_Y + (OPTION_SPACING / 2)
  end

  def options
    if state == :main_menu
      [
        'Play',
        'Sound',
        "Keys #{game_manager.controls}",
        'Quit'
      ]
    elsif state == :game_menu
      [
        'Resume',
        'Sound',
        "Keys #{game_manager.controls}",
        'Quit'
      ]
    elsif state == :sound_menu
      [
        "Music:#{sound_controller.music_enabled ? 'On' : 'off'}",
        "SFX:#{sound_controller.sounds_enabled ? 'On' : 'Off'}",
        'Back'
      ]
    end
  end

  def sound_controller
    game_manager.sound_controller
  end

  def sprites
    [
      [
        POINTER_LEFT_EDGE,
        (top - (selected_index * OPTION_SPACING)).floor,
        3,
        3,
        '/sprites/menu/menu_selector.png'
      ]
    ].tap do |arr|
      arr << [0, 0, 64, 64, "/sprites/menu/#{ctrl_state}.png"] if [:show_keys_a, :show_keys_b, :show_xbox, :show_ps4].include?(ctrl_state)
    end
  end

  def play_sound(sound)
    game_manager.sound_controller.play_sound(sound)
  end

  def background
    [0, 0, 1280, 720, 16, 16, 16]
  end
end
