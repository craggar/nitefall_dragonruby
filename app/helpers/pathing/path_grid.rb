# frozen_string_literal: true

class PathGrid
  attr_accessor :tiles, :nodes, :width, :height

  def initialize(opts = {})
    self.tiles = opts[:tiles]
    self.nodes = {}
    self.width = opts[:width]
    self.height = opts[:height]
    # debug_tiles
    parse_map
  end

  def reset
    nodes.values.map(&:values).flatten.each(&:reset_cost)
  end

  def parse_map
    width.times do |grid_x|
      height.times do |grid_y|
        tile = tiles[grid_x.to_s][grid_y.to_s]
        grid_x = tile[:grid_x]
        grid_y = tile[:grid_y]
        nodes[grid_x] ||= {}
        nodes[grid_x][grid_y] = Node.new(
          grid: self,
          grid_x: grid_x,
          grid_y: grid_y,
          cost: tile[:cost]
        )
      end
    end
  end

  def node_at(pos)
    return unless pos[:x] >= 0 &&
                  pos[:x] < width &&
                  pos[:y] >= 0 &&
                  pos[:y] < height

    nodes[pos[:x]][pos[:y]]
  end

  def debug_tiles
    lines = []
    height.times do |grid_y|
      line = []
      width.times do |grid_x|
        line << tiles[grid_x.to_s][grid_y.to_s][:cost] == 1 ? 'X' : '.'
      end
      lines << line.join()
    end
    lines.reverse.each { |line| puts line }
  end
end
