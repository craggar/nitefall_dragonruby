# frozen_string_literal: true

# Going to use this for tracking what kind of icon should be shown
class Point
  attr_accessor :grid_x, :grid_y, :cost, :next_point, :previous_point

  def self.from_node(node)
    new(node.to_param)
  end

  def initialize(opts = {})
    self.grid_x = opts[:grid_x] || 0
    self.grid_y = opts[:grid_y] || 0
    self.cost = opts[:cost]
    self.previous_point = opts[:previous_point]
    self.next_point = opts[:next_point]
  end

  def equals(other_point)
    grid_x == other_point.grid_x && grid_y == other_point.grid_y
  end

  def to_s
    @to_s ||= "(#{grid_x}, #{grid_y})"
  end
end
