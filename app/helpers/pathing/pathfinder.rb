# frozen_string_literal: true

class Pathfinder
  def self.find_path(opts = {})
    p = new(
      start: opts[:start],
      target: opts[:target],
      grid: opts[:grid],
      ignore_cost: opts[:ignore_cost].nil? ? false : opts[:ignore_cost],
      allow_diagonal: opts[:allow_diagonal].nil? ? true : opts[:allow_diagonal]
    )
    p.grid.reset
    p.find_path
  end

  attr_accessor :start, :target, :nodes, :open_set, :closed_set, :current_node,
                :grid, :path, :allow_diagonal

  def initialize(opts = {})
    self.start = opts[:start]
    self.target = opts[:target]
    self.grid = opts[:grid]
    self.allow_diagonal = opts[:allow_diagonal]
    self.nodes = []
    self.open_set = []
    self.closed_set = []
  end

  def find_path
    open_set.push start_node
    puts "start node is #{start_node || 'nil'}"
    puts "target node is #{target_node || 'nil'}"
    start_node.g_cost = 0

    until open_set.count.zero?
      next_check_node

      move_current_node_to_closed

      if current_node.equals(target_node)
        retrace_path
        return path
      end

      check_neighbours
    end
    path
  end

  def start_node
    @start_node ||= grid.node_at(x: start[:x], y: start[:y])
  end

  def target_node
    @target_node ||= grid.node_at(x: target[:x], y: target[:y])
  end

  def retrace_path
    self.path = []
    until current_node.equals(start_node)
      path.unshift Point.from_node(current_node)
      self.current_node = current_node.parent
    end
    set_relative_points
  end

  def set_relative_points
    path.each_with_index do |point, index|
      point.previous_point = path[index - 1] unless index.zero?
      point.next_point = path[index + 1] unless index == path.count - 1
    end
  end

  def next_check_node
    self.current_node = open_set[0]
    open_set.each do |check_node|
      next unless check_node.f_cost < current_node.f_cost ||
                  (check_node.f_cost == current_node.f_cost &&
                   check_node.h_cost < current_node.h_cost)

      self.current_node = check_node
    end
  end

  def check_neighbours
    current_node.neighbours(allow_diagonal).each do |neighbour|
      next if !neighbour.walkable ||
              closed_set.find { |node| node.equals(neighbour) }

      new_cost_to_neighbour = current_node.cost_to(neighbour)
      open_set_contains_neighbour = open_set.find do |node|
        node.equals(neighbour)
      end
      next if new_cost_to_neighbour > neighbour.g_cost || open_set_contains_neighbour

      update_neighbour(neighbour, new_cost_to_neighbour)
      open_set.push neighbour unless open_set_contains_neighbour
    end
  end

  def update_neighbour(node, cost)
    node.g_cost = cost
    node.h_cost = node.cost_to(target_node)
    node.parent = current_node
  end

  def move_current_node_to_closed
    self.open_set = open_set.each.reject { |node| node.equals(current_node) }
    closed_set << current_node
  end
end
