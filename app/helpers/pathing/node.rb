# frozen_string_literal: true

class Node
  attr_accessor :grid, :walkable, :grid_x, :grid_y, :cost, :g_cost, :h_cost,
                :parent

  def initialize(opts = {})
    self.grid = opts[:grid]
    self.grid_x = opts[:grid_x]
    self.grid_y = opts[:grid_y]
    self.cost = opts[:cost]
    self.walkable = opts[:walkable]
    reset_cost
    santize_walkable_cost
  end

  def reset_cost
    self.g_cost = 1_000_000_000
    self.h_cost = 0
  end

  def f_cost
    g_cost + h_cost
  end

  def cost_to(neighbour, ignore_cost = false)
    n_cost = (ignore_cost ? 1 : 10.0 * neighbour.cost)
    g_cost + distance_to(neighbour) * n_cost
  end

  def distance_to(neighbour)
    x_distance = (grid_x - neighbour.grid_x).abs
    y_distance = (grid_y - neighbour.grid_y).abs

    if x_distance > y_distance
      14 * y_distance + 10 * (x_distance - y_distance)
    else
      14 * x_distance + 10 * (y_distance - x_distance)
    end
  end

  def to_s
    @to_s ||= "(#{grid_x}, #{grid_y})"
  end

  def to_param
    {
      grid_x: grid_x,
      grid_y: grid_y,
      cost: g_cost
    }
  end

  def neighbours(allow_diagonal = true)
    (allow_diagonal ? neighbours_8d : neighbours_4d).compact
  end

  def neighbours_4d
    @neighbours_4d ||= [
      north_node,
      east_node,
      south_node,
      west_node
    ]
  end

  def neighbours_8d
    @neighbours_8d ||= neighbours_4d.tap do |neebs|
      neebs << north_east_node
      neebs << north_west_node
      neebs << south_east_node
      neebs << south_west_node
    end
  end

  def north_node
    grid.node_at(x: grid_x, y: grid_y + 1)
  end

  def north_east_node
    grid.node_at(x: grid_x + 1, y: grid_y + 1)
  end

  def east_node
    grid.node_at(x: grid_x + 1, y: grid_y)
  end

  def south_east_node
    grid.node_at(x: grid_x + 1, y: grid_y - 1)
  end

  def south_node
    grid.node_at(x: grid_x, y: grid_y - 1)
  end

  def south_west_node
    grid.node_at(x: grid_x - 1, y: grid_y - 1)
  end

  def west_node
    grid.node_at(x: grid_x - 1, y: grid_y)
  end

  def north_west_node
    grid.node_at(x: grid_x - 1, y: grid_y + 1)
  end

  def equals(other_node)
    grid_x == other_node.grid_x && grid_y == other_node.grid_y
  end

  private

  def santize_walkable_cost
    self.walkable = !cost.zero?
  end
end
