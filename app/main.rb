# frozen_string_literal: true

$gtk&.require('app/entities/abstract_entity.rb')
$gtk&.require('app/helpers/map_helper.rb')
$gtk&.require('app/entities/map_tile.rb')
$gtk&.require('app/entities/map_sector.rb')
$gtk&.require('app/entities/map.rb')

$gtk&.require('app/controllers/intro_controller.rb')
$gtk&.require('app/controllers/menu_controller.rb')
$gtk&.require('app/controllers/game_controller.rb')
$gtk&.require('app/controllers/mini_map_controller.rb')
$gtk&.require('app/controllers/win_controller.rb')
$gtk&.require('app/controllers/game_over_controller.rb')
$gtk&.require('app/controllers/sound_controller.rb')
$gtk&.require('app/controllers/inventory_controller.rb')
$gtk&.require('app/controllers/map_controller.rb')
$gtk&.require('app/controllers/dialog_controller.rb')
$gtk&.require('app/controllers/input_controller.rb')

$gtk&.require('app/helpers/pathing/point.rb')
$gtk&.require('app/helpers/pathing/node.rb')
$gtk&.require('app/helpers/pathing/path_grid.rb')
$gtk&.require('app/helpers/pathing/pathfinder.rb')

$gtk&.require('dialog/dialog.rb')

$gtk&.require('app/entities/abstract_enemy.rb')
$gtk&.require('app/entities/slime.rb')
$gtk&.require('app/entities/skeleton.rb')
$gtk&.require('app/entities/red_skeleton.rb')

$gtk&.require('app/entities/pickups/abstract.rb')
$gtk&.require('app/entities/pickups/apple.rb')
$gtk&.require('app/entities/pickups/water.rb')

$gtk&.require('app/entities/inventory.rb')
$gtk&.require('app/entities/inventory_item.rb')

$gtk&.require('app/entities/player.rb')

$gtk&.require('app/entities/tree.rb')

$gtk&.require('app/entities/weapon_hit_box.rb')
$gtk&.require('app/entities/enemy_weapon_hit_box.rb')
$gtk&.require('app/entities/player_weapon_hit_box.rb')

class GameManager
  attr_accessor :initialized,
                :intro_controller,
                :menu_controller,
                :sound_controller,
                :game_controller,
                :mini_map_controller,
                :inventory_controller,
                :active_controller,
                :win_controller,
                :game_over_controller,
                :show_framerate,
                :wants_to_quit,
                :input_choice

  # FONT = 'dragonruby-gtk-4x4.ttf'
  FONT = 'dragonruby-gtk-5x5.ttf'
  FONT_WIDTH = 5
  FONT_HEIGHT = 5
  INPUTS = ['A', 'B', 'XBOX', 'PS4'].freeze



  def tick(args)
    init unless initialized
    args.gtk.request_quit if wants_to_quit
    sprites = []
    labels = []
    if show_framerate
      current_framerate = begin
                            args&.gtk&.current_framerate
                          rescue
                            '?'
                          end
      labels << [0, 0, current_framerate.floor.to_s, 255, 255, 255]
    end

    sound_controller.process(args.outputs)
    active_controller.process(labels, sprites, args.inputs)

    args.render_target(:lowrez).solids << [0, 0, 64, 64, 255, 255, 255]
    draw_scene args, sprites, labels, active_controller.background
  end

  def init
    self.show_framerate = false
    self.sound_controller = ::SoundController.new
    self.intro_controller = IntroController.new(self)
    self.menu_controller = MenuController.new(self)
    self.game_controller = GameController.new(self)
    self.win_controller = WinController.new(self)
    self.game_over_controller = GameOverController.new(self)
    self.inventory_controller = InventoryController.new(self)
    self.mini_map_controller = MiniMapController.new(game_controller)
    self.active_controller = intro_controller
    self.wants_to_quit = false
    self.initialized = true
    self.input_choice = 0
  end

  def show_menu
    self.active_controller = menu_controller
  end

  def show_mini_map(show)
    self.active_controller = show ? mini_map_controller : game_controller
  end

  def show_inventory(show)
    self.active_controller = show ? inventory_controller : game_controller
  end

  def start_game
    game_controller.start
    self.active_controller = game_controller
  end

  def resume_game
    self.active_controller = game_controller
  end

  def victory
    game_controller.in_progress = false
    self.active_controller = win_controller
  end

  def game_over
    game_controller.in_progress = false
    self.active_controller = game_over_controller
  end

  def cycle_input
    self.input_choice = if input_choice + 1 == INPUTS.length
                          0
                        else
                          input_choice + 1
                        end
  end

  def controls
    INPUTS[input_choice]
  end

  def quit
    self.wants_to_quit = true
  end

  def game_in_progress?
    game_controller&.in_progress
  end

  def draw_scene(args, sprites, labels, background)
    # sprites.each { |s| puts s[4] if s[4]&.include?('sprites/player') }
    args.render_target(:lowrez).sprites << sprites
    store_color = nil
    args.outputs.labels << labels.map do |l|
      as_label = l.label
      label_color = { r: as_label.r, g: as_label.g, b: as_label.b }
      char_index = 0
      l.text.each_char.each.map do |char|
        i = char_index
        if char == '{'
          as_label.r = 255
          as_label.g = 0
          as_label.b = 0
          next
        elsif char == '}'
          as_label.r = label_color[:r]
          as_label.g = label_color[:g]
          as_label.b = label_color[:b]
          next
        end
        char_index += 1
        [CENTER_OFFSET + EMULATED_FONT_X_ZERO + (as_label.x * TINY_SCALE) + i * letter_spacing * TINY_SCALE,
         EMULATED_FONT_Y_ZERO + (as_label.y * TINY_SCALE), char,
         EMULATED_FONT_SIZE, 0, as_label.r, as_label.g, as_label.b, as_label.a, FONT]
      end
    end

    args.render_target(:lowrez).solids << background
    args.sprites    << [CENTER_OFFSET, 0, 1280 * TINY_SCALE, 720 * TINY_SCALE, :lowrez]
    args.primitives << [0, 0, CENTER_OFFSET, 720].solid
    args.primitives << [1280 - CENTER_OFFSET, 0, CENTER_OFFSET, 720].solid
    args.primitives << [0, 0, 1280, 2].solid
  end

  def render_mouse_crosshairs(args, mouse)
    args.labels << [10, 25, "mouse: #{mouse.x} #{mouse.y}", 255, 255, 255]
  end

  def letter_spacing
    FONT_WIDTH + 1
  end

  def line_height
    FONT_HEIGHT + 1
  end
end

###################################################################################
# YOU CAN PLAY AROUND WITH THE CODE BELOW, BUT USE CAUTION AS THIS IS WHAT EMULATES
# THE 64x64 CANVAS.
###################################################################################

TINY_RESOLUTION       = 64
TINY_SCALE            = 720.fdiv(TINY_RESOLUTION)
CENTER_OFFSET         = (1280 - 720).fdiv(2)
EMULATED_FONT_SIZE    = 17
EMULATED_FONT_X_ZERO  = 0
EMULATED_FONT_Y_ZERO  = 56

def game_manager
  @game_manager ||= GameManager.new
end

def tick(args)
  # trace!
  game_manager.tick(args)
end
