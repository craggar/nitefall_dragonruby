# frozen_string_literal: true

class Inventory
  attr_accessor :items

  def initialize(_opts = {})
    self.items = {}
  end

  def add_item(pickup, quantity = 1)
    if (item = check_bag(pickup.class.name))
      item.add_quantity(quantity)
    else
      create_item(pickup, quantity)
    end
  end

  def remove_item(item, quantity)
    return unless (item = check_bag[item.name])

    item.remove_quantity(quantity)
  end

  def check_bag(name)
    items[name]
  end

  def create_item(pickup, quantity)
    items[pickup.class.name] = InventoryItem.new(
      name: pickup.class.name,
      icon: pickup.image,
      quantity: quantity
    )
  end
end
