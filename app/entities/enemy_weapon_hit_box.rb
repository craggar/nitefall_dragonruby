# frozen_string_literal: true

class EnemyWeaponHitBox < WeaponHitBox
  def process
    super
    return unless game_controller.player.collides_with?(
      left: left,
      bottom: bottom,
      right: right,
      top: top
    )

    game_controller.player.take_damage_from(self)
  end

  def image
    '/sprites/null_sprite.png'
  end
end
