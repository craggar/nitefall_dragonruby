# frozen_string_literal: true

class RedSkeleton < AbstractEnemy
  attr_accessor :seeking

  SIGHT_RANGE = 40
  STRIKE_RANGE = 8

  def initialize(opts = {})
    super
    self.hp = 14
    self.max_hp = 14
    self.seeking = false
    self.deals_damage_on_touch = false
  end

  def collision_bounds
    {
      left: 0,
      x_mid: 4,
      right: 8,
      top: 5,
      y_mid: 2,
      bottom: 0
    }
  end

  def process(_inputs)
    super
    self.seeking = true if seeking == false && distance_to(player) < SIGHT_RANGE
    strike_towards(player) if distance_to(player) < STRIKE_RANGE
  end

  def move(_inputs)
    return unless seeking

    look_at(player)
    move_towards(player)
  end

  def load_images
    self.images = {
      walk: {
        up: [
          '/sprites/redskeleton/walk/redskeleton_walk_up_00.png',
          '/sprites/redskeleton/walk/redskeleton_walk_up_01.png',
          '/sprites/redskeleton/walk/redskeleton_walk_up_02.png',
          '/sprites/redskeleton/walk/redskeleton_walk_up_03.png'
        ],
        down: [
          '/sprites/redskeleton/walk/redskeleton_walk_down_00.png',
          '/sprites/redskeleton/walk/redskeleton_walk_down_01.png',
          '/sprites/redskeleton/walk/redskeleton_walk_down_02.png',
          '/sprites/redskeleton/walk/redskeleton_walk_down_03.png'
        ],
        left: [
          '/sprites/redskeleton/walk/redskeleton_walk_left_00.png',
          '/sprites/redskeleton/walk/redskeleton_walk_left_01.png',
          '/sprites/redskeleton/walk/redskeleton_walk_left_02.png',
          '/sprites/redskeleton/walk/redskeleton_walk_left_03.png'
        ],
        right: [
          '/sprites/redskeleton/walk/redskeleton_walk_right_00.png',
          '/sprites/redskeleton/walk/redskeleton_walk_right_01.png',
          '/sprites/redskeleton/walk/redskeleton_walk_right_02.png',
          '/sprites/redskeleton/walk/redskeleton_walk_right_03.png'
        ]
      },
      idle: {
        up: [
          '/sprites/redskeleton/idle/redskeleton_idle_down_00.png',
          '/sprites/redskeleton/idle/redskeleton_idle_down_01.png'
        ],
        down: [
          '/sprites/redskeleton/idle/redskeleton_idle_down_00.png',
          '/sprites/redskeleton/idle/redskeleton_idle_down_01.png'
        ],
        left: [
          '/sprites/redskeleton/idle/redskeleton_idle_left_00.png',
          '/sprites/redskeleton/idle/redskeleton_idle_left_01.png'
        ],
        right: [
          '/sprites/redskeleton/idle/redskeleton_idle_right_00.png',
          '/sprites/redskeleton/idle/redskeleton_idle_right_01.png'
        ]
      },
      attack: {
        up: [
          '/sprites/redskeleton/attack/redskeleton_attack_up_00.png',
          '/sprites/redskeleton/attack/redskeleton_attack_up_01.png',
          '/sprites/redskeleton/attack/redskeleton_attack_up_02.png'
        ],
        down: [
          '/sprites/redskeleton/attack/redskeleton_attack_down_00.png',
          '/sprites/redskeleton/attack/redskeleton_attack_down_01.png',
          '/sprites/redskeleton/attack/redskeleton_attack_down_02.png'
        ],
        left: [
          '/sprites/redskeleton/attack/redskeleton_attack_left_00.png',
          '/sprites/redskeleton/attack/redskeleton_attack_left_01.png',
          '/sprites/redskeleton/attack/redskeleton_attack_left_02.png'
        ],
        right: [
          '/sprites/redskeleton/attack/redskeleton_attack_right_00.png',
          '/sprites/redskeleton/attack/redskeleton_attack_right_01.png',
          '/sprites/redskeleton/attack/redskeleton_attack_right_02.png'
        ]
      }
    }
  end
end
