# frozen_string_literal: true

class MapTile < AbstractEntity
  attr_accessor :map_sector, :type, :grid_x, :grid_y

  WIDTH_IN_PIXELS = 8
  HEIGHT_IN_PIXELS = 8

  TILE_TYPES = {
    BLOCKING: 0,
    FLOOR: 1,
    PATH_EAST_WEST: 2,
    PATH_NORTH_SOUTH: 3,
    HELPER_EAST: 4,
    HELPER_WEST: 5,
    HELPER_NORTH: 6,
    HELPER_SOUTH: 7,
    TREE: 8
  }.freeze

  def initialize(opts = {})
    super
    self.map_sector = opts[:map_sector]
    self.type = opts[:type] || TILE_TYPES[:BLOCKING]
    self.grid_x = opts[:grid_x] || 0
    self.grid_y = opts[:grid_y] || 0
  end

  def cost
    type == TILE_TYPES[:BLOCKING] ? 0 : 1
  end

  def world_position
    @world_position ||= {
      x: map_sector.world_position[:x] + (grid_x * SPRITE_WIDTH),
      y: map_sector.world_position[:y] + (grid_y * SPRITE_HEIGHT)
    }
  end

  def sprite
    [draw_x, draw_y, SPRITE_WIDTH, SPRITE_HEIGHT, image]
  end

  def draw_x
    world_position[:x] - map_controller.screen_position[:x]
  end

  def draw_y
    world_position[:y] - map_controller.screen_position[:y]
  end

  def visible
    return cached_visible unless cached_visible.nil? || local_tick.zero?

    self.cached_visible = draw_x >= (SPRITE_WIDTH * -1) &&
                          draw_x <= map_controller.screen_width + SPRITE_WIDTH &&
                          draw_y >= (SPRITE_HEIGHT * -1) &&
                          draw_y <= map_controller.screen_height + SPRITE_HEIGHT
  end

  def image
    "#{images[type]}_#{map_controller.cycle}.png"
  end

  def to_s
    "GRID: (#{grid_x}, #{grid_y}), DRAW: (#{draw_x}, #{draw_y})"
  end

  def load_images
    self.images = [
      '/sprites/tiles/floor_void',
      '/sprites/tiles/floor',
      '/sprites/tiles/path_east_west',
      '/sprites/tiles/path_north_south',
      '/sprites/tiles/helper_east',
      '/sprites/tiles/helper_west',
      '/sprites/tiles/helper_north',
      '/sprites/tiles/helper_south',
      '/sprites/tiles/floor_void'
    ]
  end

  def make_helper(direction)
    if direction == 'north'
      self.type = TILE_TYPES[:HELPER_NORTH]
    elsif direction == 'east'
      self.type = TILE_TYPES[:HELPER_EAST]
    elsif direction == 'south'
      self.type = TILE_TYPES[:HELPER_SOUTH]
    elsif direction == 'west'
      self.type = TILE_TYPES[:HELPER_WEST]
    end
  end

  def blocking?
    type == TILE_TYPES[:BLOCKING] || type == TILE_TYPES[:TREE]
  end
end
