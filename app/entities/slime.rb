# frozen_string_literal: true

class Slime < AbstractEnemy
  attr_accessor :split_countdown

  MIN_SPLIT_COUNTDOWN = 300
  MAX_SPLIT_COUNTDOWN = 550

  def initialize(opts = {})
    super
    self.hp = 8
    self.max_hp = 8
    self.deals_damage_on_touch
    reset_split_countdown
  end

  def collision_bounds
    {
      left: 0,
      x_mid: 4,
      right: 8,
      top: 5,
      y_mid: 2,
      bottom: 0
    }
  end

  def move(_inputs)
    self.split_countdown -= 1
    return unless split_countdown.negative?

    split
    reset_split_countdown
  end

  def split
    north_tile = map_controller.get_tile(x: tile[:x], y: tile[:y] + 1)
    south_tile = map_controller.get_tile(x: tile[:x], y: tile[:y] - 1)
    east_tile = map_controller.get_tile(x: tile[:x] + 1, y: tile[:y])
    west_tile = map_controller.get_tile(x: tile[:x] - 1, y: tile[:y])
    valid_tiles = [north_tile, south_tile, east_tile, west_tile].compact.reject(&:blocking?)
    return unless valid_tiles.count.positive?

    new_tile = valid_tiles.sample
    game_controller.spawn_enemy(
      ::Slime,
      new_tile.world_position
    )
  end

  def reset_split_countdown
    self.split_countdown = (MIN_SPLIT_COUNTDOWN..MAX_SPLIT_COUNTDOWN).to_a.sample
  end

  def load_images
    self.images = {
      idle: {
        up: [
          '/sprites/slime/idle/slime_idle_down_00.png',
          '/sprites/slime/idle/slime_idle_down_01.png'
        ],
        down: [
          '/sprites/slime/idle/slime_idle_down_00.png',
          '/sprites/slime/idle/slime_idle_down_01.png'
        ],
        left: [
          '/sprites/slime/idle/slime_idle_left_00.png',
          '/sprites/slime/idle/slime_idle_left_01.png'
        ],
        right: [
          '/sprites/slime/idle/slime_idle_right_00.png',
          '/sprites/slime/idle/slime_idle_right_01.png'
        ]
      },
    }
  end
end
