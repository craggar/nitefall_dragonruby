# frozen_string_literal: true

class AbstractEnemy < AbstractEntity
  attr_accessor :state, :facing, :hp, :attack, :deals_damage_on_touch

  def initialize(opts = {})
    super
    self.state = :idle
    self.facing = :down
    self.attack = 1
    self.deals_damage_on_touch = true
  end

  def sprite_width
    8
  end

  def sprite_height
    8
  end

  def collision_bounds
    {
      left: 1,
      x_mid: 3,
      right: 6,
      top: 6,
      y_mid: 3,
      bottom: 1
    }
  end

  def sector
    {
      x: (world_position[:x] / (::MapSector::WIDTH_IN_TILES * MapTile::WIDTH_IN_PIXELS)).floor,
      y: (world_position[:y] / (::MapSector::HEIGHT_IN_TILES * MapTile::HEIGHT_IN_PIXELS)).floor
    }
  end

  def tile
    {
      x: (world_position[:x] / MapTile::WIDTH_IN_PIXELS).floor,
      y: (world_position[:y] / MapTile::WIDTH_IN_PIXELS).floor
    }
  end

  def sector_tile
    sector_pos = sector
    tile_pos = tile
    {
      x: tile_pos[:x] - (sector_pos[:x] * ::MapSector::WIDTH_IN_TILES),
      y: tile_pos[:y] - (sector_pos[:y] * ::MapSector::HEIGHT)
    }
  end

  def process(inputs)
    super
    return unless game_controller.nite? && map_controller.get_sector(sector)&.active

    move(inputs)
    check_collisions
    increment_local_tick
    update_image_index
  end

  def check_collisions
    if deals_damage_on_touch && player.collides_with?(collision_box)
      player.take_damage_from(self)
    end
    if player.damager&.collides_with? collision_box
      take_damage_from(player.damager)
      player.damager.damage_timer = 0
    end
  end

  def collision_box
    {
      left: x + collision_bounds[:left],
      right: x + collision_bounds[:right],
      top: y + collision_bounds[:top],
      bottom: y + collision_bounds[:bottom]
    }
  end

  def move(inputs)
    return unless (local_tick % 2).zero? && state != :attack
  end

  def update_image_index
    case state
    when :idle then update_idle_image_index
    when :walk then update_walking_image_index
    when :attack then update_attacking_image_index
    else
      self.state = :idle
      update_idle_image_index
    end
  end

  def update_idle_image_index
    self.image_index = (local_tick / 15).floor % 4
  end

  def update_walking_image_index
    self.image_index = (local_tick / 15).floor % 4
  end

  def update_attacking_image_index
    new_index = (local_tick / 15).floor % 4
    if new_index > 2
      self.state = :idle
      self.image_index = 0
    else
      self.image_index = new_index
    end
  end

  def load_images
    self.images = []
  end

  def visible
    return cached_visible unless cached_visible.nil? || local_tick.zero?

    self.cached_visible = if game_controller.day?
                            sector[:x] == game_controller.day_begin_sector.sector_x &&
                              sector[:y] == game_controller.day_begin_sector.sector_y
                          else
                            draw_x >= (SPRITE_WIDTH * -3) &&
                              draw_x <= map_controller.screen_width + (3 * SPRITE_WIDTH) &&
                              draw_y >= (SPRITE_HEIGHT * -3) &&
                              draw_y <= map_controller.screen_height + (3 * SPRITE_HEIGHT)
                          end
  end

  def player
    game_controller.player
  end

  def take_damage_from(entity)
    super

    game_controller.load_dialog_tree("Hints::#{self.class.name}") if hp.zero?
  end

  def hit_box
    EnemyWeaponHitBox
  end
end
