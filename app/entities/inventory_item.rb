# frozen_string_literal: true

class InventoryItem
  attr_accessor :quantity, :name, :icon, :limit, :affects, :effect

  def initialize(opts = {})
    self.quantity = opts[:quantity] || 0
    self.limit = opts[:limit] || -1
    self.name = opts[:name]
    self.icon = opts[:icon]
    self.affects = :hp
    self.effect = 1
  end

  def add_quantity(amount)
    self.quantity = limit.negative? ? quantity + amount : [limit, quantity + amount].min
  end

  def remove_quantity(amount)
    self.quantity = [0, quantity - amount].max
  end

  def sprite_at(position)
    [position[:x], position[:y], sprite_width, sprite_height, icon]
  end

  def formatted_quantity
    return "0#{quantity}" if quantity < 10

    "#{[99, quantity].min}"
  end

  def sprite_width
    8
  end

  def sprite_height
    8
  end
end
