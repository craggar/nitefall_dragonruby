# frozen_string_literal: true

module Pickups
  class Abstract
    attr_accessor :map_controller,
                  :game_controller,
                  :draw_depth,
                  :images,
                  :local_tick,
                  :x,
                  :y,
                  :image_index,
                  :picked_up,
                  :cached_visible

    SPRITE_WIDTH = 8
    SPRITE_HEIGHT = 8

    def initialize(opts = {})
      self.map_controller = opts[:map_controller]
      self.game_controller = opts[:game_controller]
      self.draw_depth = opts[:draw_depth] || 0
      self.x = opts[:x] || 0
      self.y = opts[:y] || 0
      self.image_index = 0
      self.local_tick = 0
      self.picked_up = false
      load_images
    end

    def process
      self.picked_up = player.collides_with?(
        left: x,
        bottom: y,
        right: x + SPRITE_WIDTH,
        top: y + SPRITE_HEIGHT
      )
    end

    def visible
      return cached_visible unless cached_visible.nil? || local_tick.zero?

      self.cached_visible = if game_controller.day?
                              sector_x == game_controller.day_begin_sector.sector_x &&
                                sector_y == game_controller.day_begin_sector.sector_y
                            else
                              draw_x >= (SPRITE_WIDTH * -3) &&
                                draw_x <= map_controller.screen_width + (3 * SPRITE_WIDTH) &&
                                draw_y >= (SPRITE_HEIGHT * -3) &&
                                draw_y <= map_controller.screen_height + (3 * SPRITE_HEIGHT)
                            end
    end

    def sector_x
      @sector_x ||= (world_position[:x] / (::MapTile::WIDTH_IN_PIXELS * ::MapSector::WIDTH_IN_TILES)).floor
    end

    def sector_y
      @sector_y ||= (world_position[:y] / (::MapTile::HEIGHT_IN_PIXELS * ::MapSector::HEIGHT_IN_TILES)).floor
    end

    def collision_bounds
      {
        left: 0,
        right: sprite_width,
        top: sprite_height,
        bottom: 0
      }
    end

    def world_position
      {
        x: x,
        y: y
      }
    end

    def blockable
      true
    end

    def draw_x
      world_position[:x] - map_controller.screen_position[:x]
    end

    def draw_y
      world_position[:y] - map_controller.screen_position[:y]
    end

    def sprite
      [draw_x, draw_y, sprite_width, sprite_height, image]
    end

    def image
      images[image_index]
    end

    def load_images
      self.images = [
        '/sprites/null_sprite.png'
      ]
    end

    def image_index_string
      image_index < 10 ? "0#{image_index}" : image_index
    end

    def increment_local_tick
      local_tick + 1 == 60 ? self.local_tick = 0 : self.local_tick += 1
    end

    def sprite_width
      SPRITE_WIDTH
    end

    def sprite_height
      SPRITE_HEIGHT
    end

    def top_right_corner
      {
        x: x + sprite_width,
        y: y + sprite_height
      }
    end

    def top_left_corner
      {
        x: x,
        y: y + sprite_height
      }
    end

    def bottom_right_corner
      {
        x: x + sprite_width,
        y: y
      }
    end

    def bottom_left_corner
      {
        x: x,
        y: y
      }
    end

    def player
      game_controller.player
    end
  end
end
