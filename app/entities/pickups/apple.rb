# frozen_string_literal: true

module Pickups
  class Apple < Pickups::Abstract
    def load_images
      self.images = [
        '/sprites/pickups/apple.png'
      ]
    end
  end
end
