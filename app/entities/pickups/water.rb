# frozen_string_literal: true

module Pickups
  class Water < Pickups::Abstract
    def load_images
      self.images = [
        '/sprites/pickups/water.png'
      ]
    end
  end
end
