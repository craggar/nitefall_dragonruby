# frozen_string_literal: true

class Player < AbstractEntity
  attr_accessor :state, :facing, :inventory, :invulnerable_timer

  INVULNERABILITY_TIME = 45

  def initialize(opts = {})
    super
    self.state = :idle
    self.facing = :down
    self.inventory = Inventory.new
    self.hp = 25
    self.max_hp = 25
    self.invulnerable_timer = 0
  end

  def sprite_width
    8
  end

  def sprite_height
    10
  end

  def collision_bounds
    {
      left: 2,
      x_mid: 3,
      right: 5,
      top: 3,
      y_mid: 1,
      bottom: 0
    }
  end

  def sector
    {
      x: (world_position[:x] / (::MapSector::WIDTH_IN_TILES * MapTile::WIDTH_IN_PIXELS)).floor,
      y: (world_position[:y] / (::MapSector::HEIGHT_IN_TILES * MapTile::HEIGHT_IN_PIXELS)).floor
    }
  end

  def process(inputs)
    super
    self.invulnerable_timer = [0, invulnerable_timer - 1].max
    move(inputs)
    increment_local_tick
    update_image_index
    update_visited_sectors
  end

  def move(inputs)
    return unless (local_tick % 2).zero?

    right_pressed = InputController.right_held?(inputs)
    left_pressed = InputController.left_held?(inputs)
    up_pressed = InputController.up_held?(inputs)
    down_pressed = InputController.down_held?(inputs)
    attack_pressed = InputController.primary_held?(inputs)

    if attack_pressed && state != :attack
      self.state = :attack
      do_a_hit
      self.local_tick = 1
    end

    return if state == :attack

    self.state = if right_pressed || left_pressed || up_pressed || down_pressed
                   :walk
                 else
                   :idle
                 end

    if up_pressed
      self.facing = :up
      move_up
    end
    if down_pressed
      self.facing = :down
      move_down
    end
    if right_pressed
      self.facing = :right
      move_right
    end
    if left_pressed
      self.facing = :left
      move_left
    end
  end

  def hit_box
    PlayerWeaponHitBox
  end

  def hit_range
    6
  end

  def strength
    2
  end

  def update_visited_sectors
    return unless (local_tick % 60).zero?

    game_controller.update_visited_sectors
  end

  def update_image_index
    case state
    when :idle then update_idle_image_index
    when :walk then update_walking_image_index
    when :attack then update_attacking_image_index
    else
      self.state = :idle
      update_idle_image_index
    end
  end

  def update_idle_image_index
    self.image_index = if local_tick < 40
                         0
                       else
                         1
                       end
  end

  def update_walking_image_index
    self.image_index = (local_tick / 15).floor % 4
  end

  def update_attacking_image_index
    new_index = (local_tick / 8).floor % 4
    if new_index > 2
      self.state = :idle
      self.image_index = 0
    else
      self.image_index = new_index
      do_a_hit if new_index == 2 && hit_again
    end
  end

  def load_images
    self.images = {
      walk: {
        up: [
          '/sprites/player/walk/player_walk_up_00.png',
          '/sprites/player/walk/player_walk_up_01.png',
          '/sprites/player/walk/player_walk_up_02.png',
          '/sprites/player/walk/player_walk_up_03.png'
        ],
        down: [
          '/sprites/player/walk/player_walk_down_00.png',
          '/sprites/player/walk/player_walk_down_01.png',
          '/sprites/player/walk/player_walk_down_02.png',
          '/sprites/player/walk/player_walk_down_03.png'
        ],
        left: [
          '/sprites/player/walk/player_walk_left_00.png',
          '/sprites/player/walk/player_walk_left_01.png',
          '/sprites/player/walk/player_walk_left_02.png',
          '/sprites/player/walk/player_walk_left_03.png'
        ],
        right: [
          '/sprites/player/walk/player_walk_right_00.png',
          '/sprites/player/walk/player_walk_right_01.png',
          '/sprites/player/walk/player_walk_right_02.png',
          '/sprites/player/walk/player_walk_right_03.png'
        ]
      },
      idle: {
        up: [
          '/sprites/player/idle/player_idle_down_00.png',
          '/sprites/player/idle/player_idle_down_01.png'
        ],
        down: [
          '/sprites/player/idle/player_idle_down_00.png',
          '/sprites/player/idle/player_idle_down_01.png'
        ],
        left: [
          '/sprites/player/idle/player_idle_left_00.png',
          '/sprites/player/idle/player_idle_left_01.png'
        ],
        right: [
          '/sprites/player/idle/player_idle_right_00.png',
          '/sprites/player/idle/player_idle_right_01.png'
        ]
      },
      attack: {
        up: [
          '/sprites/player/attack/player_attack_up_00.png',
          '/sprites/player/attack/player_attack_up_01.png',
          '/sprites/player/attack/player_attack_up_02.png'
        ],
        down: [
          '/sprites/player/attack/player_attack_down_00.png',
          '/sprites/player/attack/player_attack_down_01.png',
          '/sprites/player/attack/player_attack_down_02.png'
        ],
        left: [
          '/sprites/player/attack/player_attack_left_00.png',
          '/sprites/player/attack/player_attack_left_01.png',
          '/sprites/player/attack/player_attack_left_02.png'
        ],
        right: [
          '/sprites/player/attack/player_attack_right_00.png',
          '/sprites/player/attack/player_attack_right_01.png',
          '/sprites/player/attack/player_attack_right_02.png'
        ]
      }
    }
  end

  def labels
    labs = super
    labs
  end

  def pick_up(pickup, quantity)
    inventory.add_item(pickup, quantity)
  end

  def take_damage_from(entity)
    return unless invulnerable_timer.zero?

    super

    self.invulnerable_timer = INVULNERABILITY_TIME
  end

  def add_hp(amount)
    self.hp = [max_hp, hp + amount].min
  end

  def player?
    true
  end
end
