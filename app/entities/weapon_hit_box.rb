# frozen_string_literal: true

class WeaponHitBox < AbstractEntity
  attr_accessor :hit_box_bounds, :damage_timer, :attack

  DAMAGE_TIMER = 10

  def collision_bounds
    {
      left: hit_box_bounds[:left],
      right: hit_box_bounds[:right],
      top: hit_box_bounds[:top],
      bottom: hit_box_bounds[:bottom]
    }
  end

  def initialize(opts = {})
    super
    self.hit_box_bounds = opts[:hit_box_bounds] || default_hit_box_bounds
    self.damage_timer = DAMAGE_TIMER
    self.attack = opts[:attack] || 1
  end

  def process
    self.damage_timer = [0, damage_timer - 1].max
  end

  def default_hit_box_bounds
    {
      left: 3,
      right: 5,
      top: 5,
      bottom: 3
    }
  end

  def finished?
    damage_timer.zero?
  end
end
