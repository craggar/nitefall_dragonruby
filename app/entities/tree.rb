# frozen_string_literal: true

class Tree < MapTile
  attr_accessor :width, :height

  def initialize(opts = {})
    super(opts)
    self.draw_depth = (0..100).to_a.sample
    self.width = opts[:width] || 1
    self.height = opts[:height] || 1
  end

  def sprite_width
    12
  end

  def sprite_height
    12
  end

  def image
    '/sprites/foreground/tree.png'
  end

  def sprite
    [draw_x, draw_y, sprite_width, sprite_height, image]
  end

  def draw_x
    world_position[:x] - map_controller.screen_position[:x] - 2
  end

  def draw_y
    world_position[:y] - map_controller.screen_position[:y]
  end

  def visible
    return false unless map_sector.active

    return cached_visible unless cached_visible.nil? || local_tick.zero?

    self.cached_visible = draw_x >= (SPRITE_WIDTH * -2) &&
                          draw_x <= map_controller.screen_width + (2 * SPRITE_WIDTH) &&
                          draw_y >= (SPRITE_HEIGHT * -2) &&
                          draw_y <= map_controller.screen_height + (2 * SPRITE_HEIGHT)
  end
end
