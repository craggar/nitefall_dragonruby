# frozen_string_literal: true

class Map < AbstractEntity
  attr_accessor :width_in_sectors,
                :height_in_sectors,
                :sectors,
                :player_spawn_sector,
                :goal_sector

  def initialize(opts = {})
    super
    self.width_in_sectors = opts[:width_in_sectors] || 0
    self.height_in_sectors = opts[:height_in_sectors] || 0
    initialize_map
  end

  def initialize_map
    loop do
      generate_sectors
      set_player_spawn_sector
      set_goal_sector
      if (path = valid_player_goal_path)
        place_helper_tiles(path)
        break
      end
    end
  end

  def generate_sectors
    self.sectors = {}
    @flattened_sectors = nil
    MapHelper.generate(
      width_in_sectors,
      height_in_sectors
    ).each do |sector_x, sector_column|
      sector_column.each do |sector_y, sector_template_id|
        sectors[sector_x] ||= {}
        sectors[sector_x][sector_y] = ::MapSector.new(
          type: sector_template_id,
          sector_x: sector_x,
          sector_y: sector_y,
          map_controller: map_controller,
          game_controller: game_controller
        )
      end
    end
  end

  def set_player_spawn_sector
    sector_x = [0, width_in_sectors - 1].sample
    sector_y = [0, height_in_sectors - 1].sample
    self.player_spawn_sector = sectors[sector_x][sector_y]
    player_spawn_sector.difficulty = 0
  end

  def set_goal_sector
    loop do
      sector_x = [0, width_in_sectors - 1].sample
      sector_y = [0, height_in_sectors - 1].sample
      next if sector_x == player_spawn_sector.sector_x && sector_y == player_spawn_sector.sector_y

      self.goal_sector = sectors[sector_x][sector_y]
      break
    end
  end

  def valid_player_goal_path
    grid_tiles = {}
    flattened_sectors.each do |sector|
      tiles = MapHelper.mini_for(sector.type)
      tile_x_offset = sector.sector_x * 3
      tile_y_offset = sector.sector_y * 3
      3.times do |sector_tile_x|
        3.times do |sector_tile_y|
          grid_x = (tile_x_offset + sector_tile_x).floor
          grid_y = (tile_y_offset + sector_tile_y).floor
          grid_tiles[grid_x.to_s] ||= {}
          grid_tiles[grid_x.to_s][grid_y.to_s] = {
            grid_x: grid_x,
            grid_y: grid_y,
            cost: tiles[sector_tile_x][sector_tile_y]
          }
        end
      end
    end

    grid = PathGrid.new(
      tiles: grid_tiles,
      width: width_in_sectors * 3,
      height: height_in_sectors * 3
    )

    start_tile = {
      x: (player_spawn_sector.sector_x * 3) + 1,
      y: (player_spawn_sector.sector_y * 3) + 1
    }

    target_tile = {
      x: (goal_sector.sector_x * 3) + 1,
      y: (goal_sector.sector_y * 3) + 1
    }

    path = Pathfinder.find_path(
      start: start_tile,
      target: target_tile,
      grid: grid,
      allow_diagonal: false
    )

    return nil unless !path.nil? && path.count > (width_in_sectors + height_in_sectors - 3) * 3

    path
  end

  def place_helper_tiles(path)
    path_sectors = path.each_with_object({}) do |point, hash|
      sector_x = (point.grid_x / 3).floor
      sector_y = (point.grid_y / 3).floor
      hash["#{sector_x}_#{sector_y}"] ||= {
        x: sector_x,
        y: sector_y
      }
    end
    path_sectors.values.each_with_index do |path_sector, sector_index|
      next if sector_index == path_sectors.count - 1

      sector = sectors[path_sector[:x]][path_sector[:y]]
      next_path_sector = path_sectors.values[sector_index + 1]
      next_sector = sectors[next_path_sector[:x]][next_path_sector[:y]]
      sector.place_helper(next_sector)
    end
  end

  def sprites
    flattened_sectors.select(&:active).each_with_object([]) do |sector, tile_sprites|
      tile_sprites.concat sector.sprites
    end
  end

  def foreground_sprites
    flattened_sectors.select(&:active).each_with_object([]) do |sector, foreground_sprites|
      foreground_sprites.concat sector.tree_sprites
    end
  end

  def clear_cache
    flattened_sectors.each do |sector|
      sector.flattened_tiles.each { |t| t.cached_visible = nil }
      sector.cached_visible = nil
    end
  end

  def get_tile(position)
    sector_x = (position[:x] / ::MapSector::WIDTH_IN_TILES).floor
    sector_y = (position[:y] / ::MapSector::HEIGHT_IN_TILES).floor
    sector_tile_x = position[:x] % ::MapSector::WIDTH_IN_TILES
    sector_tile_y = position[:y] % ::MapSector::HEIGHT_IN_TILES
    return unless sector_x >= 0 && sector_x < width_in_sectors && sector_y >= 0 && sector_y < height_in_sectors

    sectors[sector_x][sector_y].get_tile(
      x: sector_tile_x,
      y: sector_tile_y
    )
  end

  def get_sector(position)
    return unless position[:x] >= 0 && position[:x] < width_in_sectors && position[:y] >= 0 && position[:y] < height_in_sectors

    sectors[position[:x]][position[:y]]
  end

  def width_in_tiles
    @width_in_tiles ||= width_in_sectors * ::MapSector::WIDTH_IN_TILES
  end

  def height_in_tiles
    @height_in_tiles ||= height_in_sectors * ::MapSector::HEIGHT_IN_TILES
  end

  def flattened_sectors
    @flattened_sectors ||= sectors.values.map(&:values).flatten
  end
end
