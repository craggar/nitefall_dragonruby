# frozen_string_literal: true

class AbstractEntity
  attr_accessor :map_controller,
                :game_controller,
                :draw_depth,
                :images,
                :local_tick,
                :x,
                :y,
                :image_index,
                :hp,
                :max_hp,
                :hit_for,
                :damage_show_timer,
                :hp_show_timer,
                :die_timer,
                :cached_visible,
                :damager,
                :hit_again

  SPRITE_WIDTH = 8
  SPRITE_HEIGHT = 8
  MOVE_SPEED = 1
  DAMAGE_SHOW_TIMER = 30
  HP_SHOW_TIMER = 60
  DIE_TIMER = 10

  SPRITE_PREFIX = {
    up: 'up',
    right: 'right',
    down: 'down',
    left: 'left'
  }.freeze

  FRAME_COUNT = {
    idle: 1,
    walk: 3,
    attack: 2
  }.freeze

  def initialize(opts = {})
    self.map_controller = opts[:map_controller]
    self.game_controller = opts[:game_controller]
    self.draw_depth = opts[:draw_depth] || 0
    self.x = opts[:x] || 0
    self.y = opts[:y] || 0
    self.image_index = 0
    self.local_tick = 0
    self.hp = 1
    self.max_hp = 1
    self.die_timer = 0
    self.damage_show_timer = 0
    self.hp_show_timer = 0
    load_images
  end

  def process(_inputs)
    self.damage_show_timer = [0, damage_show_timer - 1].max
    self.hp_show_timer = [0, hp_show_timer - 1].max
    self.die_timer = [0, die_timer - 1].max
    return if hp.zero?

    self.damager = nil if damager&.finished?
    damager.process if damager
  end

  def collision_bounds
    {
      left: 0,
      right: sprite_width,
      top: sprite_height,
      bottom: 0
    }
  end

  def world_position
    {
      x: x,
      y: y
    }
  end

  def blockable
    true
  end

  def draw_x
    world_position[:x] - map_controller.screen_position[:x]
  end

  def draw_y
    world_position[:y] - map_controller.screen_position[:y]
  end

  def labels
    labs = []
    if damage_show_timer.positive?
      damage_label_x = hit_for < 10 ? draw_x : draw_x - 4
      labs << [damage_label_x, draw_y, "-#{hit_for}", 255, 0, 0]
    end
    if hp_show_timer.positive?
      hp_label_x = if hp > 99
                     draw_x
                   elsif hp > 9
                     draw_x
                   else
                     draw_x + 4
                   end
      labs << [hp_label_x, draw_y + 6, hp.to_s, 255, 255, 255]
    end
    labs
  end

  def sprite
    [draw_x, draw_y, sprite_width, sprite_height, image]
  end

  def image
    images[state][facing][image_index] || images[state][facing][0]
  end

  def load_images
    self.images = [
      idle: {
        up: ['sprites/null_sprite.png'],
        down: ['sprites/null_sprite.png'],
        left: ['sprites/null_sprite.png'],
        right: ['sprites/null_sprite.png']
      },
      walk: {
        up: ['sprites/null_sprite.png'],
        down: ['sprites/null_sprite.png'],
        left: ['sprites/null_sprite.png'],
        right: ['sprites/null_sprite.png']
      },
      attack: {
        up: ['sprites/null_sprite.png'],
        down: ['sprites/null_sprite.png'],
        left: ['sprites/null_sprite.png'],
        right: ['sprites/null_sprite.png']
      },
    ]
  end

  def image_index_string
    image_i = [FRAME_COUNT[state], image_index].min
    "0#{image_i}"
  end

  def increment_local_tick
    local_tick + 1 == 60 ? self.local_tick = 0 : self.local_tick += 1
  end

  def sprite_width
    SPRITE_WIDTH
  end

  def sprite_height
    SPRITE_HEIGHT
  end

  def move_speed
    MOVE_SPEED
  end

  def top_right_corner
    {
      x: x + sprite_width,
      y: y + sprite_height
    }
  end

  def top_left_corner
    {
      x: x,
      y: y + sprite_height
    }
  end

  def bottom_right_corner
    {
      x: x + sprite_width,
      y: y
    }
  end

  def bottom_left_corner
    {
      x: x,
      y: y
    }
  end

  def player?
    false
  end

  def move_right
    new_x = [x + move_speed, map_controller.right - map_controller.tile_width + collision_bounds[:right] + 1].min
    check_tiles = [
      map_controller.get_tile(
        x: ((new_x + collision_bounds[:right]) / map_controller.tile_width).floor,
        y: ((y + collision_bounds[:top]) / map_controller.tile_height).floor
      ),
      map_controller.get_tile(
        x: ((new_x + collision_bounds[:right]) / map_controller.tile_width).floor,
        y: ((y + collision_bounds[:bottom]) / map_controller.tile_height).floor
      ),
      map_controller.get_tile(
        x: ((new_x + collision_bounds[:right]) / map_controller.tile_width).floor,
        y: ((y + collision_bounds[:y_mid]) / map_controller.tile_height).floor
      )
    ]

    return if check_tiles.compact.map(&:blocking?).include?(true)

    if player? && game_controller.nite?
      collisions = game_controller.enemies.select(&:visible).map do |e|
        e.collides_with?(
          left: left + 1,
          right: right + 1,
          top: top,
          bottom: bottom
        )
      end
      return if collisions.include?(true)
    end

    if game_controller.nite? || game_controller.still_in_sector(new_x, y)
      self.x = new_x
    else
      game_controller.load_dialog_tree('LeaveSectorDay')
    end
  end

  def move_left
    new_x = [x - move_speed, map_controller.left + collision_bounds[:left] + 1].max
    check_tiles = [
      map_controller.get_tile(
        x: ((new_x + collision_bounds[:left]) / map_controller.tile_width).floor,
        y: ((y + collision_bounds[:top]) / map_controller.tile_height).floor
      ),
      map_controller.get_tile(
        x: ((new_x + collision_bounds[:left]) / map_controller.tile_width).floor,
        y: ((y + collision_bounds[:bottom]) / map_controller.tile_height).floor
      ),
      map_controller.get_tile(
        x: ((new_x + collision_bounds[:left]) / map_controller.tile_width).floor,
        y: ((y + collision_bounds[:y_mid]) / map_controller.tile_height).floor
      )
    ]

    return if check_tiles.compact.map(&:blocking?).include?(true)

    if player? && game_controller.nite?
      collisions = game_controller.enemies.select(&:visible).map do |e|
        e.collides_with?(
          left: left - 1,
          right: right - 1,
          top: top,
          bottom: bottom
        )
      end
      return if collisions.include?(true)
    end

    if game_controller.nite? || game_controller.still_in_sector(new_x, y)
      self.x = new_x
    else
      game_controller.load_dialog_tree('LeaveSectorDay')
    end
  end

  def move_up
    new_y = [y + move_speed, map_controller.bottom - map_controller.tile_height - collision_bounds[:top]].min
    check_tiles = [
      map_controller.get_tile(
        x: ((x + collision_bounds[:left]) / map_controller.tile_width).floor,
        y: ((new_y + collision_bounds[:top]) / map_controller.tile_height).floor
      ),
      map_controller.get_tile(
        x: ((x + collision_bounds[:right]) / map_controller.tile_width).floor,
        y: ((new_y + collision_bounds[:top]) / map_controller.tile_height).floor
      ),
      map_controller.get_tile(
        x: ((x + collision_bounds[:x_mid]) / map_controller.tile_width).floor,
        y: ((new_y + collision_bounds[:top]) / map_controller.tile_height).floor
      )
    ]

    return if check_tiles.compact.map(&:blocking?).include?(true)

    if player? && game_controller.nite?
      collisions = game_controller.enemies.select(&:visible).map do |e|
        e.collides_with?(
          left: left,
          right: right,
          top: top + 1,
          bottom: bottom + 1
        )
      end
      return if collisions.include?(true)
    end

    if game_controller.nite? || game_controller.still_in_sector(x, new_y)
      self.y = new_y
    else
      game_controller.load_dialog_tree('LeaveSectorDay')
    end
  end

  def move_down
    new_y = [y - move_speed, map_controller.top].max
    check_tiles = [
      map_controller.get_tile(
        x: ((x + collision_bounds[:left]) / map_controller.tile_width).floor,
        y: ((new_y + collision_bounds[:bottom]) / map_controller.tile_height).floor
      ),
      map_controller.get_tile(
        x: ((x + collision_bounds[:right]) / map_controller.tile_width).floor,
        y: ((new_y + collision_bounds[:bottom]) / map_controller.tile_height).floor
      ),
      map_controller.get_tile(
        x: ((x + collision_bounds[:x_mid]) / map_controller.tile_width).floor,
        y: ((new_y + collision_bounds[:bottom]) / map_controller.tile_height).floor
      )
    ]

    return if check_tiles.compact.map(&:blocking?).include?(true)

    if player? && game_controller.nite?
      collisions = game_controller.enemies.select(&:visible).map do |e|
        e.collides_with?(
          left: left,
          right: right,
          top: top - 1,
          bottom: bottom - 1
        )
      end
      return if collisions.include?(true)
    end

    if game_controller.nite? || game_controller.still_in_sector(x, new_y)
      self.y = new_y
    else
      game_controller.load_dialog_tree('LeaveSectorDay')
    end
  end

  def collides_with?(position = {})
    bottom_right_collides = right > position[:left] &&
                            right < position[:right] &&
                            bottom > position[:bottom] &&
                            bottom < position[:top]
    top_right_collides = right > position[:left] &&
                         right < position[:right] &&
                         top > position[:bottom] &&
                         top < position[:top]
    top_left_collides = left > position[:left] &&
                        left < position[:right] &&
                        top > position[:bottom] &&
                        top < position[:top]
    bottom_left_collides = left > position[:left] &&
                           left < position[:right] &&
                           bottom > position[:bottom] &&
                           bottom < position[:top]
    loose_collision = right > position[:left] &&
                      left < position[:right] &&
                      top > position[:bottom] &&
                      bottom < position[:top]
    bottom_left_collides || bottom_right_collides || top_left_collides || top_right_collides || loose_collision
  end

  def left
    x + collision_bounds[:left]
  end

  def right
    x + collision_bounds[:right]
  end

  def top
    y + collision_bounds[:top]
  end

  def bottom
    y + collision_bounds[:bottom]
  end

  def take_damage_from(entity)
    self.hp = [hp - entity.attack, 0].max
    self.hit_for = entity.attack
    self.damage_show_timer = DAMAGE_SHOW_TIMER
    self.hp_show_timer = HP_SHOW_TIMER

    self.die_timer = DIE_TIMER unless hp.positive? || die_timer.positive?
  end

  def distance_to(entity)
    x_diff = (x - entity.x).abs
    y_diff = (y - entity.y).abs
    sum_of_squares = (x_diff * x_diff) + (y_diff * y_diff)
    Math.sqrt(sum_of_squares)
  end

  def look_at(entity)
    self.facing = :up if top < entity.bottom
    self.facing = :down if bottom > entity.top
    self.facing = :left if left > entity.right
    self.facing = :right if right < entity.left
  end

  def move_towards(entity)
    return unless (local_tick % 3).zero?

    self.x += 1 if entity.left > right
    self.x -= 1 if entity.right < left
    self.y += 1 if entity.bottom > top
    self.y -= 1 if entity.top < bottom
  end

  def strike_towards(entity)
    return if state == :attack

    look_at(entity)
    self.state = :attack
    do_a_hit
    self.local_tick = 1
  end

  def do_a_hit
    game_controller.play_sound('sounds/punch.wav')
    hit_params = if facing == :right
                   hit_right
                 elsif facing == :left
                   hit_left
                 elsif facing == :up
                   hit_up
                 elsif facing == :down
                   hit_down
                 end
    self.damager = hit_box&.new(
      hit_params.tap do |params|
        params[:attack] = strength
        params[:game_controller] = game_controller
        params[:map_controller] = map_controller
      end
    )
    self.hit_again = !hit_again
  end

  def hit_box
  end

  def hit_range
    4
  end

  def strength
    1
  end

  def hit_left
    {
      x: x - collision_bounds[:left] - hit_range,
      y: y,
      hit_box_bounds: {
        left: 0,
        right: hit_range,
        bottom: 0,
        top: SPRITE_HEIGHT
      }
    }
  end

  def hit_right
    {
      x: x + collision_bounds[:right] + 2,
      y: y,
      hit_box_bounds: {
        left: 0,
        right: hit_range,
        bottom: 0,
        top: SPRITE_HEIGHT
      }
    }
  end

  def hit_up
    {
      x: x,
      y: y + collision_bounds[:top] + 2,
      hit_box_bounds: {
        left: 0,
        right: SPRITE_WIDTH,
        bottom: 0,
        top: hit_range
      }
    }
  end

  def hit_down
    {
      x: x,
      y: y + collision_bounds[:bottom] - hit_range,
      hit_box_bounds: {
        left: 0,
        right: SPRITE_WIDTH,
        bottom: 0,
        top: hit_range
      }
    }
  end

  def dead?
    die_timer.zero? && !hp.positive?
  end

  def full_hp?
    hp == max_hp
  end
end
