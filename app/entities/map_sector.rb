# frozen_string_literal: true

class MapSector < AbstractEntity
  attr_accessor :type,
                :sector_x,
                :sector_y,
                :tiles,
                :trees,
                :helper_tiles,
                :visited,
                :difficulty

  WIDTH_IN_TILES = 16
  HEIGHT_IN_TILES = 16

  def initialize(opts = {})
    super
    self.type = opts[:type]
    self.sector_x = opts[:sector_x] || 0
    self.sector_y = opts[:sector_y] || 0
    self.trees = []
    self.helper_tiles = []
    self.visited = false
    self.difficulty = [1, 2, 3].sample
    load_tiles
  end

  def load_tiles
    self.tiles = {}
    MapHelper.send(MapHelper::LOOKUP[type]).each_with_index do |tile, tile_index|
      grid_x = tile_index % WIDTH_IN_TILES
      grid_y = (tile_index / WIDTH_IN_TILES).floor
      tiles[grid_x] ||= {}
      tiles[grid_x][grid_y] = ::MapTile.new(
        grid_x: grid_x,
        grid_y: grid_y,
        map_sector: self,
        type: tile,
        draw_depth: 0,
        map_controller: map_controller,
        game_controller: game_controller
      )
      next unless tile == 8

      trees << ::Tree.new(
        grid_x: grid_x,
        grid_y: grid_y,
        map_sector: self,
        type: tile,
        draw_depth: 0,
        map_controller: map_controller,
        game_controller: game_controller
      )
    end
  end

  def active
    return cached_visible unless cached_visible.nil? || local_tick.zero?

    self.cached_visible = if game_controller.day?
                            sector_x == game_controller.day_begin_sector.sector_x &&
                              sector_y == game_controller.day_begin_sector.sector_y
                          else
                            (sector_x >= game_controller.screen_sector[:x] - 1 &&
                              sector_x <= game_controller.screen_sector[:x] + 1) &&
                              (sector_y >= game_controller.screen_sector[:y] - 1 &&
                              sector_y <= game_controller.screen_sector[:y] + 1)
                          end
  end

  def sprites
    flattened_tiles.select(&:visible).each_with_object([]) do |tile, tile_sprites|
      tile_sprites << tile.sprite
    end
  end

  def tree_sprites
    trees.select(&:visible).sort_by(&:draw_depth).map(&:sprite)
  end

  def world_position
    @world_position ||= {
      x: sector_x * WIDTH_IN_TILES * MapTile::WIDTH_IN_PIXELS,
      y: sector_y * HEIGHT_IN_TILES * MapTile::HEIGHT_IN_PIXELS
    }
  end

  def get_tile(position)
    tiles[position[:x]][position[:y]]
  end

  def flattened_tiles
    @flattened_tiles ||= tiles.values.map(&:values).flatten
  end

  def enemy_spawn_count
    difficulty * 2
  end

  def place_helper(next_sector)
    relative_position = if next_sector.sector_x > sector_x
                          'east'
                        elsif next_sector.sector_x < sector_x
                          'west'
                        elsif next_sector.sector_y > sector_y
                          'north'
                        elsif next_sector.sector_y < sector_y
                          'south'
                        end
    if %w[east west].include? relative_position
      path_tiles = flattened_tiles.select do |t|
        t.type == MapTile::TILE_TYPES[:PATH_EAST_WEST]
      end
    elsif %w[north south].include? relative_position
      path_tiles = flattened_tiles.select do |t|
        t.type == MapTile::TILE_TYPES[:PATH_NORTH_SOUTH]
      end
    end
    path_tiles.sample.make_helper(relative_position) if path_tiles.count.positive?
    @flattened_tiles = nil
  end
end
