# frozen_string_literal: true

module Dialog
  class Abstract
    def self.entries(_opts = {})
      []
    end

    def self.and_then
      nil
    end
  end

  class Intro < ::Dialog::Abstract
    def self.entries(_opts = {})
      [
        { type: 'dialog', icon_pos: 'left', icon: '/sprites/dialog/head_left.png', text: 'Ow, my head' },
        { type: 'dialog', icon_pos: 'left', icon: '/sprites/dialog/head_left.png', text: 'That wasnt a great sleep' },
        { type: 'dialog', icon_pos: 'left', icon: '/sprites/dialog/head_left.png', text: 'Nearly nightfall, though' },
        { type: 'dialog', icon_pos: 'left', icon: '/sprites/dialog/head_left.png', text: 'Fog should clear by then' },
        { type: 'dialog', icon_pos: 'left', icon: '/sprites/dialog/head_left.png', text: 'Best get moving I guess' },
        { type: 'dialog', icon_pos: 'left', icon: '/sprites/dialog/head_left.png', text: 'But where next?' }
      ]
    end

    def self.and_then
      'Hints::Map'
    end
  end

  class Victory < ::Dialog::Abstract
    def self.entries(_opts = {})
      [
        { type: 'dialog', icon_pos: 'left', icon: '/sprites/dialog/head_left.png', text: 'The sea...' },
        { type: 'dialog', icon_pos: 'right', icon: '/sprites/dialog/head_right.png', text: 'I think I\'m safe now.' },
        { type: 'dialog', icon_pos: 'left', icon: '/sprites/dialog/head_left.png', text: 'for now...' }
      ]
    end
  end

  class GameOver < ::Dialog::Abstract
    def self.entries(_opts = {})
      [
        { type: 'dialog', icon_pos: 'left', icon: '/sprites/dialog/head_left.png', text: 'I\'ll never find my way out' },
        { type: 'dialog', icon_pos: 'left', icon: '/sprites/dialog/head_left.png', text: 'This god forsaken place..' }
      ]
    end
  end

  class LeaveSectorDay < ::Dialog::Abstract
    def self.entries(_opts = {})
      [
        { type: 'dialog', icon_pos: 'right', icon: '/sprites/dialog/head_right.png', text: 'I cant see my path in the fog' },
        { type: 'dialog', icon_pos: 'right', icon: '/sprites/dialog/head_right.png', text: 'I should stay near here' },
        { type: 'dialog', icon_pos: 'right', icon: '/sprites/dialog/head_right.png', text: 'Rest til nitefall...' }
      ]
    end
  end

  class Daybreak < ::Dialog::Abstract
    def self.entries(opts = {})
      [
        { type: 'hint', icon: '/sprites/dialog/daybreak.png', text: "Daybreak day #{opts[:day]} 08:00" }
      ]
    end
  end

  class Nitefall < ::Dialog::Abstract
    def self.entries(opts = {})
      [
        { type: 'hint', icon: '/sprites/dialog/nitefall.png', text: "Nitefall day #{opts[:day]}  20:00" }
      ]
    end
  end

  module Hints
    class Map < ::Dialog::Abstract
      def self.entries(_opts = {})
        [
          { type: 'hint', icon: '/sprites/dialog/generic_hint.png', text: 'Press {M} to view your map' }
        ]
      end
    end

    class ViewInventory < ::Dialog::Abstract
      def self.entries(_opts = {})
        [
          { type: 'hint', icon: '/sprites/dialog/generic_hint.png', text: 'Press {]} to see your inventory' }
        ]
      end
    end

    class Slime < ::Dialog::Abstract
      def self.entries(_opts = {})
        [
          { type: 'hint', icon: '/sprites/slime/idle/slime_idle_down_00.png', text: 'Slimes replicate if left untended. Kill them quickly' }
        ]
      end
    end

    class Skeleton < ::Dialog::Abstract
      def self.entries(_opts = {})
        [
          { type: 'hint', icon: '/sprites/skeleton/idle/skeleton_idle_down_00.png', text: 'Skeletons. Spooky, huh?' }
        ]
      end
    end

    class RedSkeleton < ::Dialog::Abstract
      def self.entries(_opts = {})
        [
          { type: 'hint', icon: '/sprites/redskeleton/idle/redskeleton_idle_down_00.png', text: 'Skeletons. Spooky, huh? Wait, was that thing red? Seems spookier.' }
        ]
      end
    end
  end
end
