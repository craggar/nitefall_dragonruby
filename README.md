# NiteFall - A DragonRuby project for LowRezJam2019
Note: this requires version 20 of dragonruby-gtk - earlier versions will show sprite glitches.

I'm making a public repo of my LowRezJam 2019 entry, in case it is of use to anyone. It was my first attempt at 'properly' using DragonRuby to create a "full" game.

The full game is playable on itch.io at the link below. This repo includes the full source at that state, minus the audio which I don't have the license to distribute as it was from a third party (the very excellent soundimage.org).
https://craggar.itch.io/lowrezjam2019

# Disclaimers
This is really meant to show people what DragonRuby can do. I don't claim that anything I've done is *the* right way to do things, and I didn't follow the conventions of storing things on the `$gtk.args.state` object.

# The Game
It's a really basic top-down maze-exploration brawler. 

## Player/Enemies
It features a single player, animated in 4 directions with idle, attack and walk animations. 3 enemy types: Slimes, which split and replicate if lef to their own device, skeletons (which also feature 4 direction animation with idle, attack, and walk animations) that hunt the player down (if the player comes within range) and try to attack you, and Red Skeletons which are the same but faster. Player/Enemy attacks (those that punch) generate a short lived 'hit box' that is used for collision detection.

## Inventory
There is an inventory System, though all pickups (currently an apple or water) just restor 1hp, with stacking of inventory (each item shows a 'count', rather than each individual item taking up a slot).

## Map Generation
The MapHelper defines a bunch of different prefabs - basically just squares with all the possible combinations of exits to the North, South, East and West. The map is genrated somewhat randomly with these pieces (attemping to make sure that if a 'sector' has an exit to the north, the secor above it matches that with an entrance/exit to the south, and so on. Then there is pathfinding...

## Pathfinding.
An A* implementation can be found in the `helpers/pathfinding` directory. This is only used to calculate a path from the start of the map to the end, to ensure that the game can be beaten. To simplify this pathfinding, the each sector is boild down to a 'minimap' sector (oh yeah, there's a minimap too) of 3x3 tiles to indicate which directions a sector can be entered/exited from. A minimum complexity (length of map) is used to make sure it isn't just a straight line from A to B to complete th game, but the higher the required complexity, the more 'failed' attempts at generating the map. So, the longer it takes to create a map, and increased chance of a crash/hang at looping on map generation.

## Dialog system
the GameController checks as the start of each tick if there are any dialog streams loaded in the Dialog Controller. If there is, it processes those instead of the player inputs/movement

## Sound Controller
simple sound controller - play looping background music by sending `set_music` an ogg file, play a one-time sound effect by sending a wav file to `play_sound`

## InputController
Abstracted most input logic to an InputController so the game can just query "was UP pressed, on any of the supported devices?", which will also make remapping buttons/settings keys easier. Support for Keyboard, PS4 and Xbox One controllers was tested